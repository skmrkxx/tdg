import React from 'react';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

import RoutesWithSubroutes from './RoutesWithSubroutes';
import routes from '../../constants/routes';
import { routePaths } from '../../constants/routes';

const styles = {
    byType: {

    },
    bySchema: {

    },
}


const RouterConfig = () => (
    <Router>
        <div>
            <Link to={routePaths.byType.index}>
                <Button variant="contained" color="primary">
                    Generate by Type
                </Button>
            </Link>
            <Button variant="contained" color="primary">
                Generate by Schema
							</Button>
            <Button variant="contained" color="primary">
                Generate Person Data
							</Button>
            {routes.map((route, i) => <RoutesWithSubroutes key={i} {...route} />)}
        </div>
    </Router>
);

export default withStyles(styles)(RouterConfig)