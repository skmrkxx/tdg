import React from 'react';
import { Route } from 'react-router-dom';

const RoutesWithSubroutes = route => (
    <Route 
        path={route.path}
        render={props => (
            <route.component {...props} routes={route.routes} />
        )}
    />
);

export default RoutesWithSubroutes;