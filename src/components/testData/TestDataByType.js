import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import { routePaths } from '../../constants/routes';
import RoutesWithSubroutes from '../common/RoutesWithSubroutes';


export default class TestDataByType extends Component {

	componentDidMount() {
	}

	render() {
		const { routes } = this.props;
		const ind = () => (
			<div>
				<Link to={routePaths.byType.string} >
					<Button variant='contained' color='primary'>String</Button>
				</Link>
				<Button variant='contained' color='primary'>Number</Button>
				<Button variant='contained' color='primary'>Phone Number</Button>
				<Button variant='contained' color='primary'>Name</Button>
				<Button variant='contained' color='primary'>Email</Button>
			</div>
		);

		return (
			<div>
				<Route exact path={routePaths.byType.index} render={ind} />
				{routes.map((route, i) => <RoutesWithSubroutes key={i} {...route} />)}
			</div>
		)
	}
}
