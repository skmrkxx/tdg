import TestDataByType from '../components/testData/TestDataByType';
import StringTestData from '../components/testData/StringTestData';
import GenericStringTestData from '../components/testData/GenericStringTestData';


export const routePaths = {
    byType: {
       index: '/by-type',
        get string() { return `${routePaths.byType.index}/string` },
    }
};


const routes = [
    {
        path: routePaths.byType.index,
        exact: true,
        component: TestDataByType,
        routes: [
            {
                path: routePaths.byType.string,
                component: StringTestData,
                exact: true,
            }
        ]
    },
];

export default routes;