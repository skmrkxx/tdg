export default class StringTestData {
    constructor(length) {
        this._length = length
    }

    set length(length) {
        this._length = length;
    }

    get length() {
        return this._length
    }
}