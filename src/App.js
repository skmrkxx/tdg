import React, { Component } from 'react';
import './App.css';
import { MuiThemeProvider } from '@material-ui/core/styles';

import theme from './styles/theme';
import RouterConfig from './components/common/RouterConfig';

class App extends Component {
	render() {

		return (
			<MuiThemeProvider theme={theme}>
				<div className="App">
					<RouterConfig />
				</div>
			</MuiThemeProvider>
		);
	}
}

export default App;
